package metric

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"test.mydomain.com/cache/cache"
)

func Metrics(router *mux.Router, mService UseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		appMetric := NewHTTP(r.URL.Path, r.Method)
		appMetric.Started()
		cacheRouter := cache.NewRouter(router)
		allowedOrigins := handlers.AllowedOrigins([]string{"*"})
		allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})
		handler := handlers.CORS(allowedOrigins, allowedMethods)(cacheRouter)
		handler.ServeHTTP(w, r)
		appMetric.Finished()
		appMetric.StatusCode = "200"
		mService.SaveHTTP(appMetric)
	}
}