package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"os"
	"strconv"
	"test.mydomain.com/cache/metric"
	"test.mydomain.com/cache/util"
	"time"
)

func main() {
	defSize, er := strconv.Atoi(os.Getenv("DEFAULT_CACHE_SIZE"))
	if er != nil {
		fmt.Println("INFO : Default Size not specified")
		util.Size = 10
	} else {
		util.Size = defSize
	}

	timeOutThreshold, er := strconv.Atoi(os.Getenv("CACHE_READ_TIMEOUT_THRESHOLD"))
	if er != nil {
		fmt.Println("INFO : Default Read Timeout not specified")
	} else {
		util.ReadThresholdInSec = timeOutThreshold
	}

	metricService, err1 := metric.NewPrometheusService()
	if err1 != nil {
		fmt.Println(err1.Error())
	}

	go cacheGC()

	/*router := cache.NewRouter()

	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})

	var err = http.ListenAndServe(":8080", handlers.CORS(allowedOrigins, allowedMethods)(router))
	if err != nil {
		fmt.Print("err :: ", err)
	}*/

	r := mux.NewRouter()

	r.Handle("/metrics", promhttp.Handler())

	err := http.ListenAndServe(":8080", metric.Metrics(r, metricService))
	if err != nil {
		fmt.Print("err :: ", err)
	}

}

func cacheGC() {
	interval, err := strconv.Atoi(os.Getenv("CACHE_GC_INTERVAL"))
	if err != nil {
		fmt.Println("Fatal : Invalid GC Interval Specified!!")
		interval = 5
	}
	for {
		time.Sleep(time.Duration(interval)*time.Second)
		util.GarbageCollector()
	}
}

